# This file is distributed under the Modified BSD Open Source License.
# See LICENSE.TXT for details.

cmake_minimum_required(VERSION 3.0)

function(llvm_link_libraries)
	### Given a list of LLVM components add dependencies
	### for the libraries of those components global link libraries.

	llvm_map_components_to_libnames(llvm_libs ${ARGN})
	link_libraries(${llvm_libs})
endfunction()


function(target_llvm_link_libraries TARGET)
	### Given a target and a list of LLVM components add dependencies
	### for the libraries of those components to target.

	llvm_map_components_to_libnames(llvm_libs ${ARGN})
	target_link_libraries("${TARGET}" ${llvm_libs})
endfunction()


function(add_llvm_compiler_flags)
	### Adds the compiler flags required for a project using LLVM

	add_definitions(${LLVM_DEFINITIONS})
	include_directories(${LLVM_INCLUDE_DIRS})

	## disable C++ RTTI and exceptions
	# we technically could use exceptions and rtti, but LLVM is usually compiled
	# without them and mixing these things up can cause a lot of grief.
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fno-rtti")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fno-exceptions")

	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}" PARENT_SCOPE)
endfunction()

function(target_add_llvm_compiler_flags TARGET SCOPE)
	### Adds the compiler flags required for a project using LLVM

	target_compile_definitions("${TARGET}" "${SCOPE}" ${LLVM_DEFINITIONS})
	target_include_directories("${TARGET}" "${SCOPE}" ${LLVM_INCLUDE_DIRS})

	## disable C++ RTTI and exceptions
	# we technically could use exceptions and rtti, but LLVM is usually compiled
	# without them and mixing these things up can cause a lot of grief.
	target_compile_options("${TARGET}" "${SCOPE}" -fno-rtti -fno-exceptions)
endfunction()
