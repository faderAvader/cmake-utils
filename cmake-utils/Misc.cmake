# This file is distributed under the Modified BSD Open Source License.
# See LICENSE.TXT for details.

cmake_minimum_required(VERSION 3.0)

function(add_compiler_flag FLAG)
  ### add flag for both C and C++ compiler
  set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS} ${FLAG}"   PARENT_SCOPE)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${FLAG}" PARENT_SCOPE)
endfunction()


function(add_c_flag FLAG)
	### add flag for C compiler
	set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${FLAG}" PARENT_SCOPE)
endfunction()

function(add_cxx_flag FLAG)
	### add flag for C++ compiler
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${FLAG}" PARENT_SCOPE)
endfunction()


function(find_program_or_fail VAR NAME)
	find_program(PROG "${NAME}" ${ARGN})

	if("${PROG}" STREQUAL "PROG-NOTFOUND")
		message(FATAL_ERROR "Could not find ${NAME}, try setting the CMAKE_PREFIX_PATH")
	endif()

	set("${VAR}" "${PROG}" PARENT_SCOPE)
endfunction()


function(cmake_utils_parse_arguments)
  ### a modified version of cmake_parse_arguments that does not add a prefix
  ### call like:
  ###   cmake_utils_parse_arguments(
  ###     OPTIONS ...
  ###     ONE_VALUE ...
  ###     MULTI_VALUE ...
  ###     ARGS ...
  ###   )

  cmake_parse_arguments(
    ""
    ""
    ""
    "OPTIONS;ONE_VALUE;MULTI_VALUE;ARGS"
    ${ARGN}
  )

  cmake_parse_arguments(
    "cmake_utils_parse_arguments"
    "${_OPTIONS}"
    "${_ONE_VALUE}"
    "${_MULTI_VALUE}"
    ${_ARGS}
  )

  foreach(VAR IN LISTS _OPTIONS _ONE_VALUE _MULTI_VALUE)
    set("${VAR}" "${cmake_utils_parse_arguments_${VAR}}" PARENT_SCOPE)
  endforeach(VAR)
endfunction(cmake_utils_parse_arguments)


function(enable_pretty_compiler_diagnostics)
	### make sure compiler prints fancy coloured output when run from make

	## CLANG
	if ("${CMAKE_C_COMPILER_ID}" STREQUAL "Clang")
		add_c_flag("-fcolor-diagnostics")
	endif()
	if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
		add_cxx_flag("-fcolor-diagnostics")
	endif()

	## GCC
	if ("${CMAKE_C_COMPILER_ID}" STREQUAL "GNU")
		add_c_flag("-fdiagnostics-color=always")
	endif()
	if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
		add_cxx_flag("-fdiagnostics-color=always")
	endif()

	set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}"   PARENT_SCOPE)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}" PARENT_SCOPE)
endfunction()

function(print_all_variables)
  ### print all currently defined variables

  get_cmake_property(ALL_VARS VARIABLES)
  foreach (VAR IN LISTS ALL_VARS)
    message(STATUS "${VAR}=${${VAR}}")
  endforeach()
endfunction()


function(add_simple_test NAME)
  ### simple wrapper fro add_executable and add_test.
  ### creates an executable and test with the given name and list of sources.
  ### the test command is simply the executable without arguments

  add_executable("${NAME}" "${NAME}.cpp")
  add_test(NAME "${NAME}" COMMAND "${NAME}")
endfunction(add_simple_test)


function(list_intersect VAR LIST1 LIST2)
  ### Store the intersection between the two given lists in VAR.

  set(TMP)

  foreach(ITEM IN LISTS "${LIST1}")
    list(FIND "${LIST2}" "${ITEM}" FOUND)

    if(NOT FOUND EQUAL -1)
      list(APPEND TMP "${ITEM}")
    endif()
  endforeach()

  set("${VAR}" "${TMP}" PARENT_SCOPE)
endfunction(list_intersect)
